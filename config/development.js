module.exports = {
  db: {
    mongo: 'mongodb://localhost:27017/cat',
    extraOptions: {
      server: {
        socketOptions: {
          keepAlive: 120,
          connectTimeoutMS: 30000
        }
      }
    },
    options: {
      auth: {
        authMechanism: 'MONGODB-CR'
      },
      db: {
        safe: true
      },
      server: {
        socketOptions: {
          keepAlive: 1,
          connectTimeoutMS: 30000
        }
      }
    }
  }
};