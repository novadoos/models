/*
 * Index configuration file.
 * Note: not using node-config because of:
 * https://github.com/lorenwest/node-config/issues/201
 * https://github.com/lorenwest/node-config/issues/225
 * */

const indexConfig = {
  cacheDirectory: '/tmp/cache'
};

let environmentConfig = {};
if (process.env.NODE_ENV) {
  environmentConfig = require(`./${process.env.NODE_ENV}`);
}

module.exports = Object.assign(indexConfig, environmentConfig);
