const { Schema } = require('mongoose');
let videoSchema;
let Video;

videoSchema = new Schema({
  uid: String,
  provider: String,
  name: String,
  duration: Number,
  urlSD: String,
  urlHD: String,
  deeplink: String,
  votes: Number,
  views: Number,
  rating: Number,
  thumbnail: String,
  detailedThumbnail: String,
  tags: String,
  pornstars: String,
  updatedAt: Date,
  isActive: Boolean
});

videoSchema.index({ provider: 1, uid: 1 }, { unique: true });

module.exports = (connection) => {
  if (!Video) {
    Video = connection.model('video', videoSchema);
  }

  return Video;
};
