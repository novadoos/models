const { Schema } = require('mongoose');
let AmazonListing;

const amazonListingSchema = new Schema({
  uid: String,
  title: String,
  description: String,
  manufacturerDescription: String,
  deeplink: String,
  rating: Number,
  reviews: Number,
  thumbnail: String,
  images: Schema.Types.Mixed,
  features: Schema.Types.Mixed,
  details: Schema.Types.Mixed,
  extraDetails: Schema.Types.Mixed,
  topReviews: Schema.Types.Mixed,
  updatedAt: Date,
  minPrice: Number,
  maxPrice: Number,
  isActive: Boolean
});

amazonListingSchema.index({ provider: 1, uid: 1 }, { unique: true });

module.exports = (connection) => {
  if (!AmazonListing) {
    AmazonListing = connection.model('AmazonListing', amazonListingSchema);
  }

  return AmazonListing;
};
