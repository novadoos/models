const { Schema } = require('mongoose');

let Crawler;
const STATUS_ENUM = ['running', 'killed', 'done'];
const crawlerSchema = new Schema({
  provider: String,
  pid: Number,
  status: { type: String, enum: STATUS_ENUM, required: true },
  runningSince: Date,
  lastSuccessfulRunDate: Date,
  updatedAt: { type: Date, required: true }
});

crawlerSchema.statics.run = (crawler, callback) => {
  const now = new Date();

  crawler.pid = process.pid;
  crawler.status = 'running';
  crawler.runningSince = now;
  crawler.updatedAt = now;

  crawler.save(callback);
};

crawlerSchema.statics.done = (crawler, callback) => {
  const now = new Date();

  crawler.pid = null;
  crawler.runningSince = null;
  crawler.status = 'done';
  crawler.lastSuccessfulRunDate = now;
  crawler.updatedAt = now;

  crawler.save(callback);
};

crawlerSchema.statics.kill = (crawler, callback) => {
  if (crawler.status === 'running') {
    try {
      process.kill(crawler.pid);
    } catch (e) {
      if (e.code !== 'ESRCH') return callback(e);
    }

    crawler.pid = null;
    crawler.status = 'killed';
    crawler.updatedAt = new Date();

    return crawler.save(callback);
  }

  callback();
};

module.exports = (connection) => {
  if (!Crawler) {
    Crawler = connection.model('crawlers', crawlerSchema);
  }

  return Crawler;
};
