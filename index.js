const CONF = require('./config');
const mongoose = require('mongoose');
const db = mongoose.createConnection(CONF.db.mongo, CONF.db.options);

module.exports = {
  Video: require('./lib/models/videos')(db),
  Crawler: require('./lib/models/crawler')(db),
  AmazonListing: require('./lib/models/amazon-listings')(db)
};
